import argparse
import psycopg2
from get_databases import get_databases
from datetime import datetime


def insert_databases(target_server, target_user, target_password, dba_user, dba_password):
    # Get databases from the target server
    databases = get_databases(target_server, target_user, target_password)

    # Connect to the DBA001 server
    conn_dba = psycopg2.connect(
        host='DBA001',
        user=dba_user,
        password=dba_password,
        dbname='DBA'
    )
    cursor_dba = conn_dba.cursor()

    dt = datetime.datetime.now()

    # Insert into dba.Databases table
    for db_name in databases:
        cursor_dba.execute(
                "INSERT INTO dba.databases (server_name, database_name, last_updated) VALUES (%s, %s, CURRENT_TIMESTAMP) ",
                (target_server, db_name)
            )

    # Commit changes and close connection
    conn_dba.commit()
    cursor_dba.close()
    conn_dba.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Merge database information into the DBAAdmin database.")
    parser.add_argument("target_server", help="Name of the target PostgreSQL server.")
    parser.add_argument("target_username", help="Username for the target PostgreSQL server.")
    parser.add_argument("target_password", help="Password for the target PostgreSQL server.")
    parser.add_argument("dba_username", help="Username for the DBA PostgreSQL server.")
    parser.add_argument("dba_password", help="Password for the DBA PostgreSQL server.")

    args = parser.parse_args()

    insert_databases(args.target_server, args.target_username, args.target_password, args.dba_username,
                     args.dba_password)
