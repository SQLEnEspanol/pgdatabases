# pgdatabases
The following project is used to extract information from Postgresql and insert the results in to an administrative database.

# Project Structure
The scripts assume the following:
Administrative server named DBA001
Administrative database named DBA, where all information lives

# Version History
## 2024-01-05 - Initial Commit
Added script to obtain databases
Added script to insert database information in to DBA001 server

# TODO
The following items are required and will be added over time
1. Add error handling where currently missing.
2. Add email messaging on success 
    1. Add success details, such as row counts
3. Add email messaging on failure.
    1. Add failure details, such as server name where failure occurred.  